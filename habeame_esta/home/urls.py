from django.urls import path, include
from .views import HabeasFormView

urlpatterns = [
    path('', HabeasFormView.as_view(), name='home-index-generator')
]