from django import forms

class HabeasForm(forms.Form):

    presenter_fullname = forms.CharField()
    presenter_document_type = forms.CharField()
    presenter_document_number = forms.CharField()
    presenter_address = forms.CharField()

    event_date = forms.CharField()

    detained_fullname = forms.CharField()
    detained_detention_address = forms.CharField()
    detained_detention_time = forms.TimeField()


