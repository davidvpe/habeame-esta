from .models import Habeas
from docxtpl import DocxTemplate
import io
import os
from django.conf import settings

class HabeasGenerator:
    
    data: Habeas = None

    def __init__(self, kwargs):
        self.data = Habeas(**kwargs)
        print(f"Habeas generated with data: {kwargs}")

    def generate_document(self):
        location = os.path.join(settings.BASE_DIR,'home/static/home/habeas_regular.docx')
        doc = DocxTemplate(location)
        doc.render(self.data.for_template())
        # Create in-memory buffer
        file_stream = io.BytesIO()
        # Save the .docx to the buffer
        doc.save(file_stream)
        # Reset the buffer's file-pointer to the beginning of the file
        file_stream.seek(0)

        return file_stream