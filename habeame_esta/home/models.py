from django.db import models
from datetime import date, time
import locale
# Create your models here.

class Habeas:
    def __init__(self, presenter_fullname: str, presenter_document_type: str, presenter_document_number: str, presenter_address: str, event_date: date, detained_fullname: str, detained_detention_address: str, detained_detention_time: time):
        self.presenter_fullname = presenter_fullname
        self.presenter_document_type = presenter_document_type
        self.presenter_document_number = presenter_document_number
        self.presenter_address = presenter_address
        self.event_date = event_date
        self.detained_fullname = detained_fullname
        self.detained_detention_address = detained_detention_address
        self.detained_detention_time = detained_detention_time

    def for_template(self):
        locale.setlocale(locale.LC_ALL, 'es_PE.UTF-8')
        return {
            'fecha_hoy': date.today().strftime('%d de %B'),
            'nombre_presentante': self.presenter_fullname,
            'tipo_documento': self.presenter_document_type,
            'numero_documento': self.presenter_document_number,
            'domicilio': self.presenter_address,
            'fecha_marcha': self.event_date,
            'nombre_detenido': self.detained_fullname,
            'lugar_incidente': self.detained_detention_address,
            'hora': self.detained_detention_time.strftime('%H:%M')
        }
