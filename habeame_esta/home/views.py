from django.shortcuts import render
from .forms import HabeasForm
from django.views.generic.edit import FormView
from django.views.generic import TemplateView
from django.urls import reverse_lazy
from django.http import HttpResponse

from .generator import HabeasGenerator

class HabeasFormView(FormView):
    form_class = HabeasForm
    template_name = 'home/index.html'

    def get_context_data(self, **kwargs):   
        # called whenever the pages is rendered       
        context = super().get_context_data(**kwargs)                     
        context["days_list"] = [
            '14 de Noviembre',
            '15 de Noviembre',
            '21 de Noviembre'
        ]
        return context

    def form_valid(self, form):
        return self.generate_habeas(form.cleaned_data)

    def generate_habeas(self, valid_data):
        print(valid_data)
        generator = HabeasGenerator(valid_data)
        generated_file = generator.generate_document()
        response = HttpResponse(generated_file, content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
        response['Content-Disposition'] = 'attachment; filename="habeas_corpus.docx"'
        return response