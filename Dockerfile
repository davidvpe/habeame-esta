FROM python:3.9

COPY ./habeame_esta /app/habeame_esta
COPY ./deployment /app/deployment
COPY ./requirements.txt /app/

#This is so system runs on a different timezone
RUN rm /etc/localtime
RUN ln -s /usr/share/zoneinfo/America/Lima /etc/localtime

WORKDIR /app

EXPOSE 80

RUN apt-get clean \
    && apt-get -y update
    

RUN apt-get -y install nginx \
    && apt-get -y install python3-dev \
    && apt-get -y install build-essential \
    && apt-get install -y locales \
    && sed -i -e 's/# es_PE.UTF-8 UTF-8/es_PE.UTF-8 UTF-8/' /etc/locale.gen \
    && dpkg-reconfigure --frontend=noninteractive locales

ENV LANG es_PE.UTF-8
ENV LC_ALL es_PE.UTF-8

RUN pip install virtualenv

RUN virtualenv env

COPY ./deployment/nginx.conf /etc/nginx

RUN chmod +x ./deployment/start.sh

RUN /app/env/bin/pip install -r /app/requirements.txt

CMD ["./deployment/start.sh"]
