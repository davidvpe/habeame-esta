#!/bin/bash

# Start the run once job.
echo "Docker container has been started"

source /app/env/bin/activate

cd habeame_esta

python manage.py collectstatic && service nginx start && uwsgi --ini /app/deployment/uwsgi.ini
